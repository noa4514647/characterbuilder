package com.dnd.characterbuilder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CharacterbuilderApplication {

	public static void main(String[] args) {
		SpringApplication.run(CharacterbuilderApplication.class, args);
	}

}
