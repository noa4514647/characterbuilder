package com.dnd.characterbuilder.resources;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "armor")
public class Armor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "label", length = 50)
    private String label;

    @Column(name = "type", length = 50)
    private String type;

    @Column(name = "ac")
    private Integer ac;

    @Column(name = "discretion_disadvantage")
    private Boolean discretionDisadvantage;

    @Column(name = "str_min")
    private Integer strMin;

}