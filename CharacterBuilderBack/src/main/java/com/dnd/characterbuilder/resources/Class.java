package com.dnd.characterbuilder.resources;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "class")
public class Class {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "hit_dice")
    private Integer hitDice;

    @Column(name = "subclass_level")
    private Integer subclassLevel;

    @Column(name = "label", length = 50)
    private String label;

}