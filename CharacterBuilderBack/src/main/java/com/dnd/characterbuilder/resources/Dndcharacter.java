package com.dnd.characterbuilder.resources;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@Entity
@Table(name = "dndcharacter")
public class Dndcharacter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "hp")
    private Integer hp;

    @Column(name = "initiative")
    private Integer initiative;

    @Column(name = "gp")
    private Integer gp;

    @Column(name = "sp")
    private Integer sp;

    @Column(name = "cp")
    private Integer cp;

    @Column(name = "ac")
    private Integer ac;

    @Column(name = "proficiency")
    private Integer proficiency;

    @Column(name = "xp")
    private Integer xp;

    @Column(name = "name", length = 50)
    private String name;

    @Column(name = "gender")
    private Character gender;

    @Column(name = "age")
    private Integer age;

    @Column(name = "height", precision = 3, scale = 2)
    private BigDecimal height;

    @Column(name = "weight")
    private Integer weight;

    @Column(name = "alignment", length = 50)
    private String alignment;

    @Column(name = "eyes", length = 50)
    private String eyes;

    @Column(name = "skin", length = 50)
    private String skin;

    @Column(name = "hair", length = 50)
    private String hair;

    @Lob
    @Column(name = "appearance")
    private String appearance;

    @Lob
    @Column(name = "traits")
    private String traits;

    @Lob
    @Column(name = "ideals")
    private String ideals;

    @Lob
    @Column(name = "bonds")
    private String bonds;

    @Lob
    @Column(name = "flaws")
    private String flaws;

    @Lob
    @Column(name = "backstory")
    private String backstory;

    @Lob
    @Column(name = "allies")
    private String allies;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_background", nullable = false)
    private Background idBackground;

}