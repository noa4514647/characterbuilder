package com.dnd.characterbuilder.resources;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "spell")
public class Spell {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "label", length = 50)
    private String label;

    @Column(name = "school", length = 50)
    private String school;

    @Lob
    @Column(name = "description")
    private String description;

    @Column(name = "level")
    private Integer level;

    @Column(name = "cast_time", length = 50)
    private String castTime;

    @Column(name = "spell_range")
    private Integer spellRange;

    @Column(name = "components", length = 50)
    private String components;

}