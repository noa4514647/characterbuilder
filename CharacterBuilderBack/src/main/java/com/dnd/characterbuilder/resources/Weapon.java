package com.dnd.characterbuilder.resources;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "weapon")
public class Weapon {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "label", length = 50)
    private String label;

    @Column(name = "damage", length = 50)
    private String damage;

    @Column(name = "type", length = 50)
    private String type;

    @Column(name = "damage_type", length = 50)
    private String damageType;

    @Column(name = "range_min", length = 50)
    private String rangeMin;

    @Column(name = "range_max", length = 50)
    private String rangeMax;

}