CREATE TABLE Class(
                      id INT AUTO_INCREMENT,
                      hit_dice INT,
                      subclass_level INT,
                      label VARCHAR(50),
                      PRIMARY KEY(id)
);

CREATE TABLE Race(
                     id INT AUTO_INCREMENT,
                     label VARCHAR(50),
                     speed INT,
                     PRIMARY KEY(id)
);

CREATE TABLE Item(
                     id INT AUTO_INCREMENT,
                     label VARCHAR(50),
                     description TEXT,
                     PRIMARY KEY(id)
);

CREATE TABLE Ability(
                        id INT AUTO_INCREMENT,
                        label VARCHAR(50),
                        PRIMARY KEY(id)
);

CREATE TABLE Skill(
                      id INT AUTO_INCREMENT,
                      label VARCHAR(50),
                      id_ability INT NOT NULL,
                      PRIMARY KEY(id),
                      FOREIGN KEY(id_ability) REFERENCES Ability(id)
);

CREATE TABLE Background(
                           id INT AUTO_INCREMENT,
                           label VARCHAR(50),
                           feature TEXT,
                           PRIMARY KEY(id)
);

CREATE TABLE Weapon(
                       id INT AUTO_INCREMENT,
                       label VARCHAR(50),
                       damage VARCHAR(50),
                       type VARCHAR(50),
                       damage_type VARCHAR(50),
                       range_min VARCHAR(50),
                       range_max VARCHAR(50),
                       PRIMARY KEY(id)
);

CREATE TABLE Armor(
                      id INT AUTO_INCREMENT,
                      label VARCHAR(50),
                      type VARCHAR(50),
                      ac INT,
                      discretion_disadvantage BOOLEAN,
                      str_min INT,
                      PRIMARY KEY(id)
);

CREATE TABLE Tool(
                     id INT AUTO_INCREMENT,
                     label VARCHAR(50),
                     PRIMARY KEY(id)
);

CREATE TABLE Lang(
                     id INT AUTO_INCREMENT,
                     label VARCHAR(50),
                     rarity VARCHAR(50),
                     PRIMARY KEY(id)
);

CREATE TABLE Spell(
                      id INT AUTO_INCREMENT,
                      label VARCHAR(50),
                      school VARCHAR(50),
                      description TEXT,
                      level INT,
                      cast_time VARCHAR(50),
                      spell_range INT,
                      components VARCHAR(50),
                      PRIMARY KEY(id)
);

CREATE TABLE Property(
                         id INT AUTO_INCREMENT,
                         label VARCHAR(50),
                         description TEXT,
                         PRIMARY KEY(id)
);

CREATE TABLE Feat(
                     id INT AUTO_INCREMENT,
                     label VARCHAR(50),
                     feature TEXT,
                     PRIMARY KEY(id)
);

CREATE TABLE Subclass(
                         id INT AUTO_INCREMENT,
                         label VARCHAR(50),
                         id_class INT NOT NULL,
                         PRIMARY KEY(id),
                         FOREIGN KEY(id_class) REFERENCES Class(id)
);

CREATE TABLE Feature(
                        id INT AUTO_INCREMENT,
                        label VARCHAR(50),
                        description TEXT,
                        level INT,
                        PRIMARY KEY(id)
);

CREATE TABLE DNDCharacter(
                             id INT AUTO_INCREMENT,
                             hp INT,
                             initiative INT,
                             gp INT,
                             sp INT,
                             cp INT,
                             ac INT,
                             proficiency INT,
                             xp INT,
                             name VARCHAR(50),
                             gender CHAR(1),
                             age INT,
                             height DECIMAL(3,2),
                             weight INT,
                             alignment VARCHAR(50),
                             eyes VARCHAR(50),
                             skin VARCHAR(50),
                             hair VARCHAR(50),
                             appearance TEXT,
                             traits TEXT,
                             ideals TEXT,
                             bonds TEXT,
                             flaws TEXT,
                             backstory TEXT,
                             allies TEXT,
                             id_background INT NOT NULL,
                             id_race INT NOT NULL,
                             PRIMARY KEY(id),
                             FOREIGN KEY(id_background) REFERENCES Background(id),
                             FOREIGN KEY(id_race) REFERENCES Race(id)
);

CREATE TABLE Character_Class(
                                id_class INT,
                                id_character INT,
                                level INT,
                                PRIMARY KEY(id_class, id_character),
                                FOREIGN KEY(id_class) REFERENCES Class(id),
                                FOREIGN KEY(id_character) REFERENCES DNDCharacter(id)
);

CREATE TABLE Character_Item(
                               id_character INT,
                               id_item INT,
                               quantity INT,
                               PRIMARY KEY(id_character, id_item),
                               FOREIGN KEY(id_character) REFERENCES DNDCharacter(id),
                               FOREIGN KEY(id_item) REFERENCES Item(id)
);

CREATE TABLE Character_Ability(
                                  id_character INT,
                                  id_ability INT,
                                  ability_score INT,
                                  proficiency BOOLEAN,
                                  PRIMARY KEY(id_character, id_ability),
                                  FOREIGN KEY(id_character) REFERENCES DNDCharacter(id),
                                  FOREIGN KEY(id_ability) REFERENCES Ability(id)
);

CREATE TABLE Character_Skill(
                                id_character INT,
                                id_skill INT,
                                proficiency BOOLEAN,
                                PRIMARY KEY(id_character, id_skill),
                                FOREIGN KEY(id_character) REFERENCES DNDCharacter(id),
                                FOREIGN KEY(id_skill) REFERENCES Skill(id)
);

CREATE TABLE Weapon_Ability(
                               id_ability INT,
                               id_weapon INT,
                               PRIMARY KEY(id_ability, id_weapon),
                               FOREIGN KEY(id_ability) REFERENCES Ability(id),
                               FOREIGN KEY(id_weapon) REFERENCES Weapon(id)
);

CREATE TABLE Known_Spells(
                             id_character INT,
                             id_spell INT,
                             PRIMARY KEY(id_character, id_spell),
                             FOREIGN KEY(id_character) REFERENCES DNDCharacter(id),
                             FOREIGN KEY(id_spell) REFERENCES Spell(id)
);

CREATE TABLE Weapon_Property(
                                id_weapon INT,
                                id_property INT,
                                PRIMARY KEY(id_weapon, id_property),
                                FOREIGN KEY(id_weapon) REFERENCES Weapon(id),
                                FOREIGN KEY(id_property) REFERENCES Property(id)
);

CREATE TABLE Character_Feat(
                               id_character INT,
                               id_feat INT,
                               PRIMARY KEY(id_character, id_feat),
                               FOREIGN KEY(id_character) REFERENCES DNDCharacter(id),
                               FOREIGN KEY(id_feat) REFERENCES Feat(id)
);

CREATE TABLE Character_Weapon(
                                 id_character INT,
                                 id_weapon INT,
                                 quantity INT,
                                 proficiency BOOLEAN,
                                 PRIMARY KEY(id_character, id_weapon),
                                 FOREIGN KEY(id_character) REFERENCES DNDCharacter(id),
                                 FOREIGN KEY(id_weapon) REFERENCES Weapon(id)
);

CREATE TABLE Character_Armor(
                                id_character INT,
                                id_armor INT,
                                proficiency BOOLEAN,
                                PRIMARY KEY(id_character, id_armor),
                                FOREIGN KEY(id_character) REFERENCES DNDCharacter(id),
                                FOREIGN KEY(id_armor) REFERENCES Armor(id)
);

CREATE TABLE Character_Tool(
                               id_character INT,
                               id_tool INT,
                               quantity VARCHAR(50),
                               proficiency BOOLEAN,
                               PRIMARY KEY(id_character, id_tool),
                               FOREIGN KEY(id_character) REFERENCES DNDCharacter(id),
                               FOREIGN KEY(id_tool) REFERENCES Tool(id)
);

CREATE TABLE Character_Language(
                                   id_character INT,
                                   id_language INT,
                                   PRIMARY KEY(id_character, id_language),
                                   FOREIGN KEY(id_character) REFERENCES DNDCharacter(id),
                                   FOREIGN KEY(id_language) REFERENCES Lang(id)
);

CREATE TABLE Character_Subclass(
                                   id_character INT,
                                   id_subclass INT,
                                   PRIMARY KEY(id_character, id_subclass),
                                   FOREIGN KEY(id_character) REFERENCES DNDCharacter(id),
                                   FOREIGN KEY(id_subclass) REFERENCES Subclass(id)
);

CREATE TABLE Subclass_Feature(
                                 id_subclass INT,
                                 id_feature INT,
                                 PRIMARY KEY(id_subclass, id_feature),
                                 FOREIGN KEY(id_subclass) REFERENCES Subclass(id),
                                 FOREIGN KEY(id_feature) REFERENCES Feature(id)
);

CREATE TABLE Race_Feature(
                             id_race INT,
                             id_feature INT,
                             PRIMARY KEY(id_race, id_feature),
                             FOREIGN KEY(id_race) REFERENCES Race(id),
                             FOREIGN KEY(id_feature) REFERENCES Feature(id)
);

CREATE TABLE Class_Feature(
                              id_class INT,
                              id_feature INT,
                              PRIMARY KEY(id_class, id_feature),
                              FOREIGN KEY(id_class) REFERENCES Class(id),
                              FOREIGN KEY(id_feature) REFERENCES Feature(id)
);

CREATE TABLE Class_spell(
                            id_class INT,
                            id_spell INT,
                            level INT,
                            PRIMARY KEY(id_class, id_spell),
                            FOREIGN KEY(id_class) REFERENCES Class(id),
                            FOREIGN KEY(id_spell) REFERENCES Spell(id)
);
