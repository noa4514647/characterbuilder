# CLASSES
INSERT INTO Class (label, hit_dice, subclass_level) VALUES ('Artificier', 8, 3);
INSERT INTO Class (label, hit_dice, subclass_level) VALUES ('Barbare', 12, 3);
INSERT INTO Class (label, hit_dice, subclass_level) VALUES ('Barde', 8, 3);
INSERT INTO Class (label, hit_dice, subclass_level) VALUES ('Clerc', 8, 1);
INSERT INTO Class (label, hit_dice, subclass_level) VALUES ('Druide', 6, 2);
INSERT INTO Class (label, hit_dice, subclass_level) VALUES ('Ensorceleur', 6, 1);
INSERT INTO Class (label, hit_dice, subclass_level) VALUES ('Guerrier', 10, 1);
INSERT INTO Class (label, hit_dice, subclass_level) VALUES ('Magicien', 6, 2);
INSERT INTO Class (label, hit_dice, subclass_level) VALUES ('Moine', 8, 3);
INSERT INTO Class (label, hit_dice, subclass_level) VALUES ('Occultiste', 8, 1);
INSERT INTO Class (label, hit_dice, subclass_level) VALUES ('Paladin', 10, 3);
INSERT INTO Class (label, hit_dice, subclass_level) VALUES ('Rôdeur', 10, 3);
INSERT INTO Class (label, hit_dice, subclass_level) VALUES ('Roublard', 8, 3);

# RACES
INSERT INTO Race (label, speed) VALUES ('Haut Elfe', 9);
INSERT INTO Race (label, speed) VALUES ('Elfe des bois', 10.5);
INSERT INTO Race (label, speed) VALUES ('Drow', 9);
INSERT INTO Race (label, speed) VALUES ('Halfelin pied-léger', 7.5);
INSERT INTO Race (label, speed) VALUES ('Halfelin robuste', 7.5);
INSERT INTO Race (label, speed) VALUES ('Humain', 9);
INSERT INTO Race (label, speed) VALUES ('Nain des collines', 7.5);
INSERT INTO Race (label, speed) VALUES ('Nain des montagnes', 7.5);
INSERT INTO Race (label, speed) VALUES ('Demi-elfe', 9);
INSERT INTO Race (label, speed) VALUES ('Demi-Orc', 9);
INSERT INTO Race (label, speed) VALUES ('Drakéide d''airin', 9);
INSERT INTO Race (label, speed) VALUES ('Drakéide d''argent', 9);
INSERT INTO Race (label, speed) VALUES ('Drakéide de bronze', 9);
INSERT INTO Race (label, speed) VALUES ('Drakéide de cuivre', 9);
INSERT INTO Race (label, speed) VALUES ('Drakéide d''or', 9);
INSERT INTO Race (label, speed) VALUES ('Gnome des forêts', 7.5);
INSERT INTO Race (label, speed) VALUES ('Gnome des roches', 7.5);
INSERT INTO Race (label, speed) VALUES ('Tieffelin', 9);

# SUBCLASSES
INSERT INTO Subclass (label, id_class) VALUES ('Alchimiste', 1);
INSERT INTO Subclass (label, id_class) VALUES ('Artilleur', 1);
INSERT INTO Subclass (label, id_class) VALUES ('Forgeron de guerre', 1);
INSERT INTO Subclass (label, id_class) VALUES ('Voie du berserker', 2);
INSERT INTO Subclass (label, id_class) VALUES ('Voie du guerrier totem', 2);
INSERT INTO Subclass (label, id_class) VALUES ('Collège du savoir', 3);
INSERT INTO Subclass (label, id_class) VALUES ('Collège de la vaillance', 3);
INSERT INTO Subclass (label, id_class) VALUES ('Domaine de la duperie', 4);
INSERT INTO Subclass (label, id_class) VALUES ('Domaine de la guerre', 4);
INSERT INTO Subclass (label, id_class) VALUES ('Domaine de la lumière', 4);
INSERT INTO Subclass (label, id_class) VALUES ('Domaine de la nature', 4);
INSERT INTO Subclass (label, id_class) VALUES ('Domaine du savoir', 4);
INSERT INTO Subclass (label, id_class) VALUES ('Domaine de la tempête', 4);
INSERT INTO Subclass (label, id_class) VALUES ('Domaine de la vie', 4);
INSERT INTO Subclass (label, id_class) VALUES ('Cercle de la lune', 5);
INSERT INTO Subclass (label, id_class) VALUES ('Cercle de la terre', 5);
INSERT INTO Subclass (label, id_class) VALUES ('Lignée draconique', 6);
INSERT INTO Subclass (label, id_class) VALUES ('Magie sauvage', 6);
INSERT INTO Subclass (label, id_class) VALUES ('Champion', 7);
INSERT INTO Subclass (label, id_class) VALUES ('Maître de guerre', 7);
INSERT INTO Subclass (label, id_class) VALUES ('Chevalier occulte', 7);
INSERT INTO Subclass (label, id_class) VALUES ('Ecole d''abjuration', 8);
INSERT INTO Subclass (label, id_class) VALUES ('Ecole de divination', 8);
INSERT INTO Subclass (label, id_class) VALUES ('Ecole d''enchantement', 8);
INSERT INTO Subclass (label, id_class) VALUES ('Ecole d''évocation', 8);
INSERT INTO Subclass (label, id_class) VALUES ('Ecole d''invocation', 8);
INSERT INTO Subclass (label, id_class) VALUES ('Ecole d''illusion', 8);
INSERT INTO Subclass (label, id_class) VALUES ('Ecole de nécromancie', 8);
INSERT INTO Subclass (label, id_class) VALUES ('Ecole de transmutation', 8);
INSERT INTO Subclass (label, id_class) VALUES ('Voie de la paume', 9);
INSERT INTO Subclass (label, id_class) VALUES ('Voie de l''ombre', 9);
INSERT INTO Subclass (label, id_class) VALUES ('Voie des quatre éléments', 9);
INSERT INTO Subclass (label, id_class) VALUES ('Fiélon', 10);
INSERT INTO Subclass (label, id_class) VALUES ('Archifée', 10);
INSERT INTO Subclass (label, id_class) VALUES ('Grand ancien', 10);
INSERT INTO Subclass (label, id_class) VALUES ('Serment de dévotion', 11);
INSERT INTO Subclass (label, id_class) VALUES ('Serment des anciens', 11);
INSERT INTO Subclass (label, id_class) VALUES ('Serment de vengeance', 11);
INSERT INTO Subclass (label, id_class) VALUES ('Conclave des bêtes', 12);
INSERT INTO Subclass (label, id_class) VALUES ('Conclave des chasseurs', 12);
INSERT INTO Subclass (label, id_class) VALUES ('Conclave des profondeurs', 12);
INSERT INTO Subclass (label, id_class) VALUES ('Assassin', 13);
INSERT INTO Subclass (label, id_class) VALUES ('Voleur', 13);
INSERT INTO Subclass (label, id_class) VALUES ('Escroc arcanique', 13);

# LANGUAGES
INSERT INTO Lang (label, rarity) VALUES ('Commun', 'standard');
INSERT INTO Lang (label, rarity) VALUES ('Elfe', 'standard');
INSERT INTO Lang (label, rarity) VALUES ('Géant', 'standard');
INSERT INTO Lang (label, rarity) VALUES ('Gnome', 'standard');
INSERT INTO Lang (label, rarity) VALUES ('Gobelin', 'standard');
INSERT INTO Lang (label, rarity) VALUES ('Halfelin', 'standard');
INSERT INTO Lang (label, rarity) VALUES ('Nain', 'standard');
INSERT INTO Lang (label, rarity) VALUES ('Orc', 'standard');
INSERT INTO Lang (label, rarity) VALUES ('Abyssal', 'exotic');
INSERT INTO Lang (label, rarity) VALUES ('Céleste', 'exotic');
INSERT INTO Lang (label, rarity) VALUES ('Commun des profondeurs', 'exotic');
INSERT INTO Lang (label, rarity) VALUES ('Draconique', 'exotic');
INSERT INTO Lang (label, rarity) VALUES ('Infernal', 'exotic');
INSERT INTO Lang (label, rarity) VALUES ('Primordial', 'exotic');
INSERT INTO Lang (label, rarity) VALUES ('Profond', 'exotic');
INSERT INTO Lang (label, rarity) VALUES ('Sylvestre', 'exotic');

# ITEMS
INSERT INTO Item (label, description) VALUES ('Corde en chanvre (15 m)', 'Une corde, qu''elle soit faite de chanvre ou de soie, possède 2 pv et peut être cassée avec un jet de Force DD 17.');

# TOOLS
INSERT INTO Tool (label) VALUES ('Outils de forgeron');

# ARMORS
INSERT INTO Armor (label, type, ac, discretion_disadvantage, str_min) VALUES ('Armure en cuir', 'Light', 11, false, 0);
INSERT INTO Armor (label, type, ac, discretion_disadvantage, str_min) VALUES ('Armure en écailles', 'Medium', 14, true,  0);
INSERT INTO Armor (label, type, ac, discretion_disadvantage, str_min) VALUES ('Cotte de mailles', 'Heavy', 16, true, 13);

# WEAPONS
INSERT INTO Weapon (label, damage, damage_type, range_min, range_max, type) VALUES ('Javeline', '1d6', 'Perforant', '0', '1.5', 'simple');
INSERT INTO Weapon (label, damage, damage_type, range_min, range_max, type) VALUES ('Rapière', '1d8', 'Perforant', '0', '1.5', 'martial');
INSERT INTO Weapon (label, damage, damage_type, range_min, range_max, type) VALUES ('Epée longue', '1d8', 'Tranchant', '0', '1.5', 'martial');

# PROPERTIES
INSERT INTO Property (label, description) VALUES ('Finesse', 'Lorsque vous effectuez une attaque avec une arme de finesse, vous pouvez au choix appliquer votre modificateur de Force ou celui de Dextérité à vos jets d''attaque et de dégâts. Le même modificateur s''applique aux deux jets.');
INSERT INTO Property (label, description) VALUES ('Polyvalente (1d10)', 'Cette arme peut être tenue à une ou deux mains. Le chiffre indiqué entre parenthèses correspond aux dégâts si l''arme est tenue à deux mains lors d''une attaque au corps à corps.');
INSERT INTO Property (label, description) VALUES ('Lancer (portée 6/18)', 'Une arme qui possède la propriété lancer peut être lancée pour réaliser une attaque à distance. Si l''arme est une arme de corps à corps, vous utilisez la même caractéristique pour le jet d''attaque et de dégâts que vous auriez utilisée au corps à corps.');

# ABILITIES
INSERT INTO Ability (label) VALUES ('Force');
INSERT INTO Ability (label) VALUES ('Dextérité');
INSERT INTO Ability (label) VALUES ('Constitution');
INSERT INTO Ability (label) VALUES ('Intelligence');
INSERT INTO Ability (label) VALUES ('Sagesse');
INSERT INTO Ability (label) VALUES ('Charisme');

# SKILLS
INSERT INTO Skill (label, id_ability) VALUES ('Acrobaties', 2);
INSERT INTO Skill (label, id_ability) VALUES ('Arcanes', 4);
INSERT INTO Skill (label, id_ability) VALUES ('Athlétisme', 1);
INSERT INTO Skill (label, id_ability) VALUES ('Discrétion', 2);
INSERT INTO Skill (label, id_ability) VALUES ('Dressage', 5);
INSERT INTO Skill (label, id_ability) VALUES ('Escamotage', 2);
INSERT INTO Skill (label, id_ability) VALUES ('Histoire', 4);
INSERT INTO Skill (label, id_ability) VALUES ('Intimidation', 6);
INSERT INTO Skill (label, id_ability) VALUES ('Intuition', 5);
INSERT INTO Skill (label, id_ability) VALUES ('Investigation', 4);
INSERT INTO Skill (label, id_ability) VALUES ('Médecine', 5);
INSERT INTO Skill (label, id_ability) VALUES ('Nature', 4);
INSERT INTO Skill (label, id_ability) VALUES ('Perception', 5);
INSERT INTO Skill (label, id_ability) VALUES ('Persuasion', 6);
INSERT INTO Skill (label, id_ability) VALUES ('Religion', 4);
INSERT INTO Skill (label, id_ability) VALUES ('Représentation', 6);
INSERT INTO Skill (label, id_ability) VALUES ('Survie', 5);
INSERT INTO Skill (label, id_ability) VALUES ('Tromperie', 6);

# SPELLS
INSERT INTO Spell (label, school, description, level, spell_range, cast_time, components) VALUES ('Contact glacial', 'Nécromancie', 'Vous créez une main squelettique fantomatique dans l''espace d''une créature à portée. Faites une attaque à distance avec un sort contre la créature pour l''étreindre d''une froideur sépulcrale. Si le coup touche, la cible subit 1d8 dégâts nécrotiques, et elle ne peut récupérer ses points de vie avant le début de votre prochain tour. Jusque-là, la main fantomatique s''accroche à la cible. Si vous ciblez un mort-vivant, il aura également un désavantage à ses jets d''attaque contre vous jusqu''à la fin de votre prochain tour.
Les dégâts de ce sort augmentent de 1d8 lorsque vous atteignez le niveau 5 (2d8), le niveau 11 (3d8) et le niveau 17 (4d8).', 0, 36, 'action', 'V, S');

# BACKGROUNDS
INSERT INTO Background (label, feature) VALUES ('Acolyte', 'En tant qu''acolyte, vous imposez le respect à ceux qui partagent votre foi, et pouvez mener les cérémonies religieuses de votre divinité. Vous et vos compagnons aventuriers pouvez vous attendre à recevoir des soins gratuits dans un temple, un sanctuaire ou toute autre présence établie de votre foi, bien que vous soyez responsable de fournir toutes les composantes matérielles nécessaires aux sorts. Ceux qui partagent votre religion vous aideront (mais seulement vous) et vous offriront un mode de vie modeste.
Vous pouvez également avoir des liens avec un temple spécifique dédié à votre divinité ou votre panthéon, et vous y avez une résidence. Cela pourrait être le temple où vous avez servi, si vous êtes resté en bons termes avec lui, ou un temple dans lequel vous avez trouvé une nouvelle maison. Lorsque vous êtes proche de votre temple, vous pouvez faire appel à des religieux pour vous aider, à condition que l''aide que vous demandiez ne soit pas dangereuse et que vous restiez en règle avec votre temple.');

# FEATS
INSERT INTO Feat (label, feature) VALUES ('Chanceux', 'Vous avez une chance inexplicable qui semble tomber juste au bon moment.
Vous avez 3 points de chance. Chaque fois que vous faites un jet d''attaque, de caractéristique ou de sauvegarde, vous pouvez dépenser un point de chance pour lancer un d20 supplémentaire. Vous pouvez choisir de dépenser un de vos points de chance après avoir lancé le dé mais avant que son effet soit déterminé. Vous choisissez quel d20 utiliser pour le jet d''attaque, de caractéristique ou de sauvegarde.
Vous pouvez également dépenser un point de chance quand un jet d''attaque est fait contre vous. Lancez un d20, et choisissez quel jet l''attaquant doit utiliser.
Si plus d''une créature dépense un point de chance pour influencer le résultat d''un jet, les points s''annulent ; aucun dé supplémentaire n''est lancé.
Vous regagnez vos points de chance dépensés lorsque vous terminez un repos long.');

# FEATURES
INSERT INTO Feature (label, description, level) VALUES ('Source de magie', 'Au niveau 2, vous puisez dans une source profonde de magie en vous. Cette source est représentée par des points de sorcellerie qui vous permettent de créer une variété d''effets magiques.', 2);
INSERT INTO Feature (label, description, level) VALUES ('Résistance infernale', 'Vous avez la résistance aux dégâts de feu.', 1);
INSERT INTO Feature (label, description, level) VALUES ('Pic de magie sauvage', 'Dès lors que vous choisissez cette origine au niveau 1, vos sorts peuvent déclencher des poussées de magie incontrôlables. Une fois par tour, le MD peut vous demander de lancer un d20, immédiatement après que vous ayez lancé un sort d''ensorceleur du niveau 1 ou plus. Si vous obtenez un 1, vous déclenchez un effet magique aléatoire. Reportez-vous au tableau ci-dessous de Pic de magie sauvage pour déterminer les effets du résultat. Si l''effet de la magie sauvage est un sort, celui-ci est trop violent pour être affecté par votre métamagie. S''il demande normalement de la concentration, dans ce cas il n''en demande pas ; le sort persiste pour sa durée totale.', 1);

# RELATIONS
INSERT INTO Weapon_Property (id_weapon, id_property) VALUES (1, 4);
INSERT INTO Weapon_Property (id_weapon, id_property) VALUES (2, 2);
INSERT INTO Weapon_Property (id_weapon, id_property) VALUES (3, 3);
INSERT INTO Weapon_Ability (id_weapon, id_ability) VALUES (1, 1);
INSERT INTO Weapon_Ability (id_weapon, id_ability) VALUES (2, 1);
INSERT INTO Weapon_Ability (id_weapon, id_ability) VALUES (2, 2);
INSERT INTO Weapon_Ability (id_weapon, id_ability) VALUES (3, 1);
INSERT INTO Class_feature (id_class, id_feature) VALUES (6, 1);
INSERT INTO Subclass_feature (id_subclass, id_feature) VALUES (18, 3);
INSERT INTO Race_feature (id_race, id_feature) VALUES (18, 2);
INSERT INTO Class_spell (id_class, id_spell) VALUES (6, 1);
INSERT INTO Class_spell (id_class, id_spell) VALUES (8, 1);
INSERT INTO Class_spell (id_class, id_spell) VALUES (10, 1);